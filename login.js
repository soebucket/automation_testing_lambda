'use strict';

exports.handler = async (event, context, callback) => {
    console.log('Login Event', event);
    require('./driver').handler({ url: event.url, isDriver: true }, {}, async (res) => {
        const { By } = require('selenium-webdriver')
        if (res.status === "Error") {
            callback(res)
            return
        }
        let driver = res.driver
        try {
            await driver.findElement(By.name(event.selector.email)).sendKeys(event.value.email)
            await driver.findElement(By.name(event.selector.password)).sendKeys(event.value.password)
            await driver.sleep(1000)
            await driver.findElement(By.className(event.selector.loginButton)).click()
        } catch (e) {
            console.log("error debug1.1");
            callback({
                status: "Error",
                message: e,
            })
            return
        }
        let currentUrl
        try {
            currentUrl = await driver.getCurrentUrl()
        } catch (e) {
            console.log('Driver get currrent url error', e);
        }

        console.log('Driver get currrent url', currentUrl);
        console.log('Event current url', event.url);
        if (currentUrl === event.url) {
            callback({
                status: "Error",
                message: JSON.stringify("Login Failed")
            })
            return
        }
        callback({
            status: "Ok",
            data: event.isDriver ? driver : JSON.stringify("Login success")
        })
    })
}

