'use strict';
exports.handler = async (event, context, callback) => {
    //import 
    const webdriver = require('selenium-webdriver');
    const chrome = require('selenium-webdriver/chrome');
    const path = require('path')

    // set chromedriver path
    const chromeDriverPath = path.join(__dirname, "bin/chromedriver");
    var service = new chrome.ServiceBuilder(chromeDriverPath)

    // build selenium web driver with chromedriver service
    var builder = new webdriver.Builder()
        .setChromeService(service)
        .forBrowser('chrome')

    // chrome options set attriubutes
    var chromeOptions = new chrome.Options();
    const defaultChromeFlags = [
        '--headless',
        '--disable-gpu',
        '--window-size=1280x1696', // Letter size
        '--no-sandbox',
        '--hide-scrollbars',
        '--enable-logging',
        '--log-level=0',
        '--v=99',
        '-disable-dev-shm-usage',
        '--ignore-certificate-errors',
         // '--single-process',
        //'--data-path=/tmp/data-path',
        //'--homedir=/tmp',
        //'--disk-cache-dir=/tmp/cache-dir',
        // '--user-data-dir=/tmp/user-data',
    ];

    //set chrome options to binary path
    chromeOptions.setChromeBinaryPath(path.join(__dirname, "bin/headless-chromium"));

    //set chrome args
    chromeOptions.addArguments(defaultChromeFlags);

    // set chrome flags options to builder 
    builder.setChromeOptions(chromeOptions);
    let driver

    try {
        driver = await builder.build();
        await driver.get(event.url);
        let title = await driver.getTitle()
        console.log('Page title', title);
    } catch (e) {
        console.error(e)
        callback({
            status: "Error",
            message: e,
        })
        return
    }
    callback({
        status: 'Ok',
        driver: driver
    })
}

