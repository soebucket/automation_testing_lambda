const mock = require('./mock.json')

//check educate myanmar 
require('./driver').handler({ url: mock.rootUrl }, {}, (res) => {
    res.status !== "Error" ? () => {
        console.log("dirver success")
        res.driver.quit();
    }
        :
        console.log(res);
})

// test login 
let login = Object.assign(mock.login,
    {
        value:
        {
            email: "soe.thu.mail.com@gmail.com",
            password: "soe@thu@mail@com"
        }
    })
require('./login').handler(login, {}, (res) => {
    console.log("<==== Test Login ====>", res);
})


// test signup

let singup = Object.assign(mock.signup,
    {
        value:
        {
            name: "Soe Thu",
            email: "soe.thu.mail.com@gmail.com",
            password: "soe@thu@mail@com",
            confirmPassword: "soe@thu@mail@com",
            phoneNumber: "09425041493",
            city: "Yangon",
        }
    })
require('./signup').handler(singup, {}, (res) => {
    console.log("<==== Test Signup ====>", res);
})

// test hub

require('./index').handler({}, {}, (res) => {
    console.log('<==== Test Hub ====>', res);
})