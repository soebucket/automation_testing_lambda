'use strict';
const mock = require('./mock.json')
const os = require('os')
exports.handler = async (event, context, callback) => {
    if (event.isLogin) {
        let login = Object.assign(mock.login,
            {
                value:
                {
                    email: event.email,
                    password: event.password,
                }
            })
        require('./login').handler(login, {}, (res) => {
            callback(JSON.stringify(res))
            return
        })
    }
    if (event.isSignup) {
        let singup = Object.assign(mock.signup,
            {
                value:
                {
                    name: event.name,
                    email: event.email,
                    password: event.password,
                    confirmPassword: event.confirm_password,
                    phoneNumber: event.phone_number,
                    city: event.city,
                }
            })
        require('./signup').handler(singup, {}, (res) => {
            callback(JSON.stringify(res))
            return
        })
    }
    let data = { "Vesion": 1.0, "Freee Mem": os.freemem(), "Total Mem": os.totalmem() }
    callback(JSON.stringify({
        status: "Ok",
        data: data
    }))
};

