'use strict';

exports.handler = async (event, context, callback) => {
    console.log('Signup Event', event);
    require('./driver').handler({ url: event.url, isDriver: true }, {}, async (res) => {
        const { By } = require('selenium-webdriver')
        if (res.status === "Error") {
            callback(res)
            return
        }
        let driver = res.driver
        try {
            await driver.findElement(By.name(event.selector.name)).sendKeys(event.value.name)
            await driver.findElement(By.name(event.selector.email)).sendKeys(event.value.email)
            await driver.findElement(By.name(event.selector.password)).sendKeys(event.value.password)
            await driver.findElement(By.name(event.selector.confirmPassword)).sendKeys(event.value.confirmPassword)
            await driver.findElement(By.name(event.selector.phoneNumber)).sendKeys(event.value.phoneNumber)
            var _city = await driver.findElement(By.name(event.selector.city))
            await _city.click()
            await _city.sendKeys(event.value.city)
            await driver.findElement(By.className(event.selector.signUpButton)).click()
        } catch (e) {
            console.log("singup debug1.1");
            callback({
                status: "Error",
                message: e,
            })
            return
        }
        let currentUrl
        try {
            currentUrl = await driver.getCurrentUrl()
        } catch (e) {
            console.log('Driver get currrent url error', e);
        }

        console.log('Driver get currrent url', currentUrl);
        console.log('Event current url', event.url);
        if (currentUrl === event.url) {
            callback({
                status: "Error",
                message: JSON.stringify("Registration Failed")
            })
            return
        }
        callback({
            status: "Ok",
            data: event.isDriver ? driver : JSON.stringify("Registration success")
        })
    })
}
